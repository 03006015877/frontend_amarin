﻿

using App4.Models;
using Refit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App4.Interface
{
    interface IMyAPI
    {
        [Get("/api/v1/hospital?emergency_list=true")]
        Task<List<ServerParameter>> GetUsers();
    }
}