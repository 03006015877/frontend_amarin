﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace App4
{
    [Activity(Label = "ProvidersList")]
    public class ProvidersList : Activity
    {
        RecyclerView mRecyclerView, recycler_view1;
        RecyclerView.LayoutManager mLayoutManager;
        LinearLayoutManager linearLayoutManager;
        Adapter.HospitalAdapter mAdapter;
        Adapter.ImageAdapter imageAdapter;
        ProvidersList providersList;
        readonly List<Models.HospitalsList> hospitalsLists=new List<Models.HospitalsList>();
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.providers_list);
            mRecyclerView= FindViewById<RecyclerView>(Resource.Id.recycler_view2);
            recycler_view1 = FindViewById<RecyclerView>(Resource.Id.recycler_view1);
            providersList = this;
            Models.HospitalsList hospitalsList = new Models.HospitalsList();
            Models.HospitalsList hospitals = hospitalsList;
            hospitals.Caption = "Hmc Emergency Department";
          
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);

            linearLayoutManager = new LinearLayoutManager(this,0,false);
            recycler_view1.SetLayoutManager(linearLayoutManager);
            imageAdapter = new Adapter.ImageAdapter(hospitalsLists);
            recycler_view1.SetAdapter(imageAdapter);
            SnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.AttachToRecyclerView(recycler_view1);

            mLayoutManager = new GridLayoutManager(this,2);
            mRecyclerView.SetLayoutManager(mLayoutManager);
            mAdapter = new Adapter.HospitalAdapter(providersList,hospitalsLists);
            mRecyclerView.SetAdapter(mAdapter);

        }
    
    }
}