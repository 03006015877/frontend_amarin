﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace App4
{
    [Activity(Label = "QuestionsActivity")]
    public class QuestionsActivity : Activity
    {
      
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            SetContentView(Resource.Layout.question_screen);
            TextView above_btn = FindViewById<TextView>(Resource.Id.above_btn);
            above_btn.Click += aboveOnClick;

            TextView under_btn = FindViewById<TextView>(Resource.Id.under_btn);
            under_btn.Click += underOnClick;
        }
        private void underOnClick(object sender, EventArgs eventArgs)
        {
            StartActivity(typeof(UnderActivity));
        }
        private void aboveOnClick(object sender, EventArgs eventArgs)
        {
            StartActivity(typeof(AboveActivity));
        }
    }
}