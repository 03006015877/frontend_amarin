﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace App4
{
    [Activity(Label = "EmergencyProviders")]
    public class DepartmentsofEmergencyProviders : Activity
    {
        LinearLayoutManager linearLayoutManager;
        RecyclerView mRecyclerView;
        Adapter.DepartmentListAdapter mAdapter;
        readonly List<Models.HospitalsList> hospitalsLists = new List<Models.HospitalsList>();
        DepartmentsofEmergencyProviders emergencyProviders;
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_emergency_providers);
            emergencyProviders = this;
            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view2);
            Models.HospitalsList hospitalsList = new Models.HospitalsList();
            Models.HospitalsList hospitals = hospitalsList;
            hospitals.Caption = "Hmc Emergency Department";

            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);

            linearLayoutManager = new LinearLayoutManager(this, 1, false);
            mRecyclerView.SetLayoutManager(linearLayoutManager);
            mAdapter = new Adapter.DepartmentListAdapter(emergencyProviders, hospitalsLists);
            mRecyclerView.SetAdapter(mAdapter);
        }
    }
}