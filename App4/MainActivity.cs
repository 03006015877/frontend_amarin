﻿using System;
using System.Collections.Generic;
using System.IO;
using Android.App;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using App4.Interface;
using App4.Models;
using Java.Lang;
using Org.Json;
using Refit;
using SQLite;


namespace App4
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        RecyclerView  recycler_view1;
        readonly List<Models.HospitalsList> hospitalsLists = new List<Models.HospitalsList>();
        LinearLayoutManager linearLayoutManager;
        Adapter.ImageAdapter imageAdapter;
        IMyAPI myAPI;
        protected override async void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.activity_main);
           
           
             recycler_view1 = FindViewById<RecyclerView>(Resource.Id.recycler_view1);


            getParaAsync();

            Models.HospitalsList hospitalsList = new Models.HospitalsList();
            Models.HospitalsList hospitals = hospitalsList;
            hospitals.Caption = "Hmc Emergency Department";
   

            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);

            linearLayoutManager = new LinearLayoutManager(this, 0, false);
            recycler_view1.SetLayoutManager(linearLayoutManager);
            imageAdapter = new Adapter.ImageAdapter(hospitalsLists);
            recycler_view1.SetAdapter(imageAdapter);
            SnapHelper snapHelper = new PagerSnapHelper();
            snapHelper.AttachToRecyclerView(recycler_view1);
            TextView search_btn = FindViewById<TextView>(Resource.Id.text);
            search_btn.Click += FabOnClick;
           
            Button find_btn = FindViewById<Button>(Resource.Id.find_btn);
            find_btn.Click += FindOnClick;
            JSONObject jSONObject = new JSONObject();
            jSONObject.Put("emergency_list", "true");
           
        }

       
        private void FindOnClick(object sender, EventArgs eventArgs)
        {
            StartActivity(typeof(QuestionsActivity));
        }
        private void FabOnClick(object sender, EventArgs eventArgs)
        {
            StartActivity(typeof(ProvidersList));
        }
        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }




        public async System.Threading.Tasks.Task getParaAsync() {
            Toast.MakeText(this, "gdgsg" , ToastLength.Long).Show();
            try
            {
                myAPI = RestService.For<IMyAPI>("https://glacial-brushlands-87794.herokuapp.com");
                ServerParameter server = new ServerParameter();
                server.emergency_list = "true";
                Toast.MakeText(this, "gdgsg" + await myAPI.GetUsers(), ToastLength.Long).Show();
            }
            catch (System.Exception e) {
                Toast.MakeText(this, "gdgsg" + e, ToastLength.Long).Show();
            }
           
        
           
        }

        public string CreateDB()
        {
            var output = "";
            output += "Creating Databse if it doesnt exists";
            string dpPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "user.db3"); //Create New Database  
            var db = new SQLiteConnection(dpPath);
            output += "\n Database Created....";
            return output;
        }
        public string InsertIntoDB(JavaList<HospitalsList> hospitalsLists)
        {
            System.String records = "";
            try
            {
                string dpPath = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "user.db3");
                var db = new SQLiteConnection(dpPath);
                db.CreateTable<HospitalsList>();
                for (int ind=0;ind<hospitalsLists.Size();ind++) {
                    db.Insert(hospitalsLists.Get(ind));
                }
               return records;

            }
            catch (System.Exception ex)
            {
                return ex+"";
               
            }
        }

      
    }
   
}

