﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using App4.Models;

namespace App4.Adapter
{
     class HospitalAdapter : RecyclerView.Adapter
    {
        public List<HospitalsList>  mPhotoAlbum;
        ProvidersList providersList;
        public HospitalAdapter(ProvidersList providersList,List<HospitalsList> photoAlbum)
        {
            mPhotoAlbum = photoAlbum;
            this.providersList = providersList;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }

        public override void
       OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            PhotoViewHolder vh = holder as PhotoViewHolder;
      
            vh.Caption.Text = mPhotoAlbum[position].Caption;
        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.providers_item, parent, false);
            PhotoViewHolder vh = new PhotoViewHolder(itemView);

            vh.Caption.Click += (sender, e) => {
                providersList. StartActivity(typeof(DepartmentsofEmergencyProviders));
            };
            

            return vh;
        }
    }
    public class PhotoViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
        public TextView Caption { get; private set; }
        public object Name { get; internal set; }

        public PhotoViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
            Image = itemView.FindViewById<ImageView>(Resource.Id.image);
            Caption = itemView.FindViewById<TextView>(Resource.Id.desc);
        }
    }
    
}