﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using App4.Models;

namespace App4.Adapter
{
    class ServiceListAdaper : RecyclerView.Adapter
    {
        public List<HospitalsList> mPhotoAlbum;
        HospitalDetailActivity providersList;
        public ServiceListAdaper(HospitalDetailActivity departmentList, List<HospitalsList> photoAlbum)
        {
            mPhotoAlbum = photoAlbum;
            this.providersList = departmentList;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }

        public override void
       OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            ServiceListAdaperViewHolder vh = holder as ServiceListAdaperViewHolder;
            vh.Caption.Text = mPhotoAlbum[position].Caption;
            vh.Caption.Click += (sender, e) => {
               // providersList.StartActivity(typeof(DepartmentsofEmergencyProviders));
            };

        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.services_item, parent, false);
            ServiceListAdaperViewHolder vh = new ServiceListAdaperViewHolder(itemView);
            return vh;
        }
    }
    public class ServiceListAdaperViewHolder : RecyclerView.ViewHolder
    {
       
        public TextView Caption { get; private set; }

        public ServiceListAdaperViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
    
            Caption = itemView.FindViewById<TextView>(Resource.Id.name);
        }
    }

}