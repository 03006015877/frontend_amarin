﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using App4.Models;

namespace App4.Adapter
{
    class DepartmentListAdapter : RecyclerView.Adapter
    {
        public List<HospitalsList> mPhotoAlbum;
        readonly DepartmentsofEmergencyProviders providersList;
        public DepartmentListAdapter(DepartmentsofEmergencyProviders departmentList, List<HospitalsList> photoAlbum)
        {
            mPhotoAlbum = photoAlbum;
            this.providersList = departmentList;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }

        public override void
       OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            DepartmentListViewHolder vh = holder as DepartmentListViewHolder;
            vh.Caption.Text = mPhotoAlbum[position].Caption;
            vh.Caption.Click += (sender, e) => {
               providersList.StartActivity(typeof(HospitalDetailActivity));
            };

        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.activity_emergency_providers_item, parent, false);
            DepartmentListViewHolder vh = new DepartmentListViewHolder(itemView);
            return vh;
        }
    }
    public class DepartmentListViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Arrow { get; private set; }
        public TextView Caption { get; private set; }

        public DepartmentListViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
            Arrow = itemView.FindViewById<ImageView>(Resource.Id.arrow);
            Caption = itemView.FindViewById<TextView>(Resource.Id.name);
        }
    }

}