﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using App4.Models;

namespace App4.Adapter
{
    class ImageAdapter : RecyclerView.Adapter
    {
        public List<HospitalsList> mPhotoAlbum;

        public ImageAdapter(List<HospitalsList> photoAlbum)
        {
            mPhotoAlbum = photoAlbum;
        }

        public override int ItemCount
        {
            get { return mPhotoAlbum.Count; }
        }

        public override void
       OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
        {
            PhotoViewHolder vh = holder as PhotoViewHolder;

           
        }


        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.image_item, parent, false);
            PhotoViewHolder vh = new PhotoViewHolder(itemView);
            return vh;
        }
    }
    public class ImageAdapterViewHolder : RecyclerView.ViewHolder
    {
        public ImageView Image { get; private set; }
    

        public ImageAdapterViewHolder(View itemView) : base(itemView)
        {
            // Locate and cache view references:
            Image = itemView.FindViewById<ImageView>(Resource.Id.image);
           }
    }
}