﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Gms.Maps;
using Android.Gms.Maps.Model;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace App4
{
    [Activity(Label = "HospitalDetailActivity")]
    class HospitalDetailActivity : Activity, IOnMapReadyCallback
    {
        GridLayoutManager linearLayoutManager;
        RecyclerView mRecyclerView;
        Adapter.ServiceListAdaper mAdapter;
        readonly List<Models.HospitalsList> hospitalsLists = new List<Models.HospitalsList>();
        HospitalDetailActivity hospitalDetailActivity;

        [Obsolete]
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            SetContentView(Resource.Layout.hospital_detail_activity);
            hospitalDetailActivity = this;
            mRecyclerView = FindViewById<RecyclerView>(Resource.Id.recycler_view2);
            Models.HospitalsList hospitalsList = new Models.HospitalsList();
            Models.HospitalsList hospitals = hospitalsList;
            hospitals.Caption = "Service-1";

            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);
            hospitalsLists.Add(hospitals);

            var mapFragment = (MapFragment)FragmentManager.FindFragmentById(Resource.Id.map);
            mapFragment.GetMapAsync(this);



            linearLayoutManager = new GridLayoutManager(this, 3);
            mRecyclerView.SetLayoutManager(linearLayoutManager);
            mAdapter = new Adapter.ServiceListAdaper(hospitalDetailActivity, hospitalsLists);
            mRecyclerView.SetAdapter(mAdapter);
        }

        public void OnMapReady(GoogleMap googleMap)
        {

            LatLng location = new LatLng(50.897778, 3.013333);

            CameraPosition.Builder builder = CameraPosition.InvokeBuilder();
            builder.Target(location);
            builder.Zoom(18);
            builder.Bearing(155);
            builder.Tilt(65);

            CameraPosition cameraPosition = builder.Build();

            CameraUpdate cameraUpdate = CameraUpdateFactory.NewCameraPosition(cameraPosition);

            googleMap.MoveCamera(cameraUpdate);
        }
    }
}